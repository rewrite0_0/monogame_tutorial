﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Tutorial
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Shooter : Game
    {
        GraphicsDeviceManager graphics;
        //SpriteBatch spriteBatch;

        SpriteBatch _spriteBatch;
        Player player;
        KeyboardState currentKeyboardState;

        KeyboardState previousKeyboardState;

        MouseState currentMouseState;

        MouseState previousMouseState;
        float playerMoveSpeed;


        public Shooter()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            player = new Player();
            
            // TODO: Add your initialization logic here
            playerMoveSpeed = 8.0f;
   

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            Vector2 playerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X, GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);

            player.Initialize(Content.Load<Texture2D>("player"), playerPosition);
            // Load the player resources


            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // Use the Keyboard / Dpad

            if (currentKeyboardState.IsKeyDown(Keys.Left))

            {

                player.Position.X -= playerMoveSpeed;

            }



            if (currentKeyboardState.IsKeyDown(Keys.Right))

            {

                player.Position.X += playerMoveSpeed;

            }



            if (currentKeyboardState.IsKeyDown(Keys.Up))

            {

                player.Position.Y -= playerMoveSpeed;

            }



            if (currentKeyboardState.IsKeyDown(Keys.Down))

            {

                player.Position.Y += playerMoveSpeed;

            }



            // Make sure that the player does not go out of bounds

            player.Position.X = MathHelper.Clamp(player.Position.X, 0, GraphicsDevice.Viewport.Width - player.Width);

            player.Position.Y = MathHelper.Clamp(player.Position.Y, 0, GraphicsDevice.Viewport.Height - player.Height);

            // TODO: Add your update logic here
            

            previousKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            // Start drawing

            _spriteBatch.Begin();



            // Draw the Player

            player.Draw(_spriteBatch);



            // Stop drawing

            _spriteBatch.End();


          
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
