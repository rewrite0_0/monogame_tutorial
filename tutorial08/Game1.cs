﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace tutorial08
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch _spriteBatch;
        Player player;
        KeyboardState currentKeyboardState;
        KeyboardState previousKeyboardState;
        float playerMoveSpeed;

        Texture2D mainBackground;

        Rectangle rectBackground;

        Rectangle laserRectangle;

        float scale = 1f;

        ParallaxingBackground bgLayer1;

        ParallaxingBackground bgLayer2;

        // texture to hold the laser.
        Texture2D laserTexture;
        List<Laser> laserBeams;

        // govern how fast our laser can fire.
        TimeSpan laserSpawnTime;
        TimeSpan previousLaserSpawnTime;


        // The rate at which enemies appear.
        TimeSpan enemySpawnTime;
        TimeSpan previousSpawnTime;
        //Enemies
        Texture2D enemyTexture;
        List<Enemy> enemies;

        // Collections of explosions
        List<Explosion> explosions;

        //Texture to hold explosion animation.
        Texture2D explosionTexture;
        //Our Laser Sound and Instance
        private SoundEffect laserSound;
        private SoundEffectInstance laserSoundInstance;

        //Our Explosion Sound.
        private SoundEffect explosionSound;
        private SoundEffectInstance explosionSoundInstance;

        /* Game Music */
        private Song gameMusic;
        Random random;

        protected void UpdateEnemies(GameTime gameTime)
        {
            // spawn a new enemy every 1.5 seconds.
            if (gameTime.TotalGameTime - previousSpawnTime > enemySpawnTime)
            {
                previousSpawnTime = gameTime.TotalGameTime;

                // add an enemy
                AddEnemy();
            }

            // update the enemies
            for (var i = 0; i < enemies.Count; i++)
            {
                enemies[i].Update(gameTime);
                if (!enemies[i].Active)
                {
                    enemies.Remove(enemies[i]);
                }
            }
        }

        protected void AddEnemy()
        {
            // create the animation object
            Animation enemyAnimation = new Animation();

            // Init the animation with the correct 
            // animation information
            enemyAnimation.Initialize(enemyTexture,
                Vector2.Zero,
                47,
                61,
                8,
                30,
                Color.White,
                1f,
                true);

            // randomly generate the postion of the enemy
            Vector2 position = new Vector2(
                GraphicsDevice.Viewport.Width + enemyTexture.Width / 2,
                random.Next(100, GraphicsDevice.Viewport.Height - 100));

            // create an enemy
            Enemy enemy = new Enemy();

            // init the enemy
            enemy.Initialize(enemyAnimation, position);

            // Add the enemy to the active enemies list
            enemies.Add(enemy);

        }



        protected void FireLaser(GameTime gameTime)
        {
            // govern the rate of fire for our lasers
            if (gameTime.TotalGameTime - previousLaserSpawnTime > laserSpawnTime)
            {
                previousLaserSpawnTime = gameTime.TotalGameTime;
                // Add the laer to our list.
                AddLaser();
                // Play the laser sound!

                laserSoundInstance.Play();

            }
        }

        protected void AddLaser()
        {
            Animation laserAnimation = new Animation();
            // initlize the laser animation
            laserAnimation.Initialize(laserTexture,
                player.Position,
                46,
                16,
                1,
                30,
                Color.White,
                1f,
                true);

            Laser laser = new Laser();
            // Get the starting postion of the laser.

            var laserPostion = player.Position;
            // Adjust the position slightly to match the muzzle of the cannon.
            laserPostion.Y += 37;
            laserPostion.X += 70;

            // init the laser
            laser.Initialize(laserAnimation, laserPostion);
            laserBeams.Add(laser);
            /* todo: add code to create a laser. */
            // laserSoundInstance.Play();
        }


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            player = new Player();
            // TODO: Add your initialization logic here
            playerMoveSpeed = 8.0f;
            mainBackground = Content.Load<Texture2D>("mainbackground");

            rectBackground = new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            bgLayer1 = new ParallaxingBackground();

            bgLayer2 = new ParallaxingBackground();
            laserTexture = Content.Load<Texture2D>("laser");
            laserBeams = new List<Laser>();
            const float SECONDS_IN_MINUTE = 60f;
            const float RATE_OF_FIRE = 200f;
            laserSpawnTime = TimeSpan.FromSeconds(SECONDS_IN_MINUTE / RATE_OF_FIRE);
            previousLaserSpawnTime = TimeSpan.Zero;

           

            // Initialize the enemies list
            enemies = new List<Enemy>();

            //used to determine how fast the enemies will respawn.
            enemySpawnTime = TimeSpan.FromSeconds(1.0f);

            // init our random number generator
            random = new Random();

            explosions = new List<Explosion>();


            base.Initialize();
        }




        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            // Load the player resources

            Animation playerAnimation = new Animation();

            Texture2D playerTexture = Content.Load<Texture2D>("shipAnimation");
            mainBackground = Content.Load<Texture2D>("mainbackground");
            playerAnimation.Initialize(playerTexture, Vector2.Zero, 115, 69, 8, 30, Color.White, 1f, true);



            Vector2 playerPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.X,

            GraphicsDevice.Viewport.TitleSafeArea.Y

            + GraphicsDevice.Viewport.TitleSafeArea.Height / 2);

            player.Initialize(playerAnimation, playerPosition);
            // TODO: use this.Content to load your game content here

            bgLayer1.Initialize(Content, "bgLayer1", GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, -1);

            bgLayer2.Initialize(Content, "bgLayer2", GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, -2);

            // load the enemy texture.
            enemyTexture = Content.Load<Texture2D>("mineAnimation");

            // load the explosion sheet
            explosionTexture = Content.Load<Texture2D>("explosion");
            // Load the laserSound Effect and create the effect Instance
            // Load the laserSound Effect and create the effect Instance
            laserSound = Content.Load<SoundEffect>("Sounds\\laserFire");
            laserSoundInstance = laserSound.CreateInstance();

            // Load the laserSound Effect and create the effect Instance
            explosionSound = Content.Load<SoundEffect>("Sounds\\explosion");
            explosionSoundInstance = explosionSound.CreateInstance();

            // Load the game music
            gameMusic = Content.Load<Song>("Sounds\\gameMusic");

            // Start playing the music.
            MediaPlayer.Play(gameMusic);



        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (currentKeyboardState.IsKeyDown(Keys.Left))

            {

                player.Position.X -= playerMoveSpeed;

            }



            if (currentKeyboardState.IsKeyDown(Keys.Right))

            {

                player.Position.X += playerMoveSpeed;

            }



            if (currentKeyboardState.IsKeyDown(Keys.Up))

            {

                player.Position.Y -= playerMoveSpeed;

            }



            if (currentKeyboardState.IsKeyDown(Keys.Down))

            {

                player.Position.Y += playerMoveSpeed;

            }


            if (currentKeyboardState.IsKeyDown(Keys.Z))
            {
                FireLaser(gameTime);
            }
            // update laserbeams
            for (var i = 0; i < laserBeams.Count; i++)
            {
                laserBeams[i].Update(gameTime);
                // Remove the beam when its deactivated or is at the end of the screen.
                if (!laserBeams[i].Active || laserBeams[i].Position.X > GraphicsDevice.Viewport.Width)
                {
                    laserBeams.Remove(laserBeams[i]);
                }
            }


            // Make sure that the player does not go out of bounds

            player.Position.X = MathHelper.Clamp(player.Position.X, 0, GraphicsDevice.Viewport.Width - player.Width);

            player.Position.Y = MathHelper.Clamp(player.Position.Y, 0, GraphicsDevice.Viewport.Height - player.Height);

            // TODO: Add your update logic here


            previousKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            // TODO: Add your update logic here
            player.Update(gameTime);
            bgLayer1.Update(gameTime);

            bgLayer2.Update(gameTime);


            // update lasers
            //UpdateLasers(gameTime);

            // update the enemies
            UpdateEnemies(gameTime);

            // update collisons
            UpdateCollision();
            UpdateExplosions(gameTime);

            base.Update(gameTime);
        }



        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            _spriteBatch.Begin();


            _spriteBatch.Draw(mainBackground, rectBackground, Color.White);
            // TODO: Add your drawing code here
            bgLayer1.Draw(_spriteBatch);
            bgLayer2.Draw(_spriteBatch);
            player.Draw(_spriteBatch);

            // Draw the lasers.
            foreach (var l in laserBeams)
            {
                l.Draw(_spriteBatch);
            }


            // draw the enemies
            foreach (var e in enemies)
            {
                e.Draw(_spriteBatch);
            };

            foreach (var e in explosions)
            {
                e.Draw(_spriteBatch);
            }

            _spriteBatch.End();


            base.Draw(gameTime);
        }


        private void UpdateExplosions(GameTime gameTime)
        {
            for (var e = 0; e < explosions.Count; e++)
            {
                explosions[e].Update(gameTime);

                if (!explosions[e].Active)
                    explosions.Remove(explosions[e]);
            }
        }
        protected void AddExplosion(Vector2 enemyPosition)
        {
            Animation explosionAnimation = new Animation();

            explosionAnimation.Initialize(explosionTexture,
                enemyPosition,
                134,
                134,
                12,
                30,
                Color.White,
                1.0f,
                true);

            Explosion explosion = new Explosion();
            explosion.Initialize(explosionAnimation, enemyPosition);

            explosions.Add(explosion);
            /* play the explosion sound. */
            explosionSound.Play();
            //explosionSoundInstance.Play();
        }


        protected void UpdateCollions()
        {

        }

        protected void UpdateCollision()
        {

            // we are going to use the rectangle's built in intersection
            // methods.

            Rectangle playerRectangle;
            Rectangle enemyRectangle;
            Rectangle laserRectangle;

            // create the rectangle for the player
            playerRectangle = new Rectangle(
                (int)player.Position.X,
                (int)player.Position.Y,
                player.Width,
                player.Height);

            // detect collisions between the player and all enemies.
            for (var i = 0; i < enemies.Count; i++)
            {
                enemyRectangle = new Rectangle(
                   (int)enemies[i].Position.X,
                   (int)enemies[i].Position.Y,
                   enemies[i].Width,
                   enemies[i].Height);

                // determine if the player and the enemy intersect.
                if (playerRectangle.Intersects(enemyRectangle))
                {

                    // kill off the enemy
                    enemies[i].Health = 0;
                    enemies[i].Health = 0;
                    // Show the explosion where the enemy was...
                    AddExplosion(enemies[i].Position);
                    
                    // deal damge to the player
                    player.Health -= enemies[i].Damage;

                    // if the player has no health destroy it.
                    if (player.Health <= 0)
                    {
                        player.Active = false;
                    }
                }

                for (var l = 0; l < laserBeams.Count; l++)
                {
                    // create a rectangle for this laserbeam
                    laserRectangle = new Rectangle(
                        (int)laserBeams[l].Position.X,
                        (int)laserBeams[l].Position.Y,
                        laserBeams[l].Width,
                        laserBeams[l].Height);

                    // test the bounds of the laer and enemy
                    if (laserRectangle.Intersects(enemyRectangle))
                    {

                        // Show the explosion where the enemy was...
                        AddExplosion(enemies[i].Position);
                        // kill off the enemy
                        enemies[i].Health = 0;

                        // kill off the laserbeam
                        laserBeams[l].Active = false;
                    }
                }

                




            }
        }
    }
}
